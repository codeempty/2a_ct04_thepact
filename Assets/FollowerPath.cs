﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowerPath : MonoBehaviour
{

    [Range(0.15f, 1.0f)]
    public float LengthDraw;
    public float TimePath;
    public float SpeedFollower;

    ParticleSystem partSystem;
    ParticleSystem.MainModule mainPart;
    MegaPathFollow mgFollow;
    private bool started = false;

    void Awake ()
    {
        partSystem = transform.GetComponentInChildren<ParticleSystem>();
        mainPart = partSystem.main;
        mgFollow = this.GetComponent<MegaPathFollow>();

        mainPart.duration = TimePath;
        mainPart.startLifetime = TimePath * LengthDraw;

        mgFollow.curve = 13;
        mgFollow.alpha = 0.0f;
        mgFollow.time = TimePath;
        mgFollow.ctime = 0.0f;
        mgFollow.speed = SpeedFollower;
        
    }

    void Start()
    {
        partSystem.Play();
        started = true;
    }

    void Update() {
        if (started) {
            if (mgFollow.alpha >= 99.0f )
            {
                mgFollow.alpha = 0.0f;
                mgFollow.speed = 0.0f;
                mgFollow.distance = 0.0f;
                mgFollow.time = 0;
                mgFollow.ctime = 0.0f;
                partSystem.Stop();
            }
        }
    }
}
