﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowerShape : MonoBehaviour
{

    [Range(0.15f, 1.0f)]
    public float LengthDraw;
    public float TimePath;

    ParticleSystem partSystem;
    ParticleSystem.MainModule mainPart;
    MegaShapeFollow mgFollow;
    private bool started = false;

    void Awake ()
    {
        partSystem = transform.GetComponentInChildren<ParticleSystem>();
        mainPart = partSystem.main;
        mgFollow = this.GetComponent<MegaShapeFollow>();

        mainPart.duration = TimePath;
        mainPart.startLifetime = TimePath * LengthDraw;
        mgFollow.distance = 0.0f;
        mgFollow.time = TimePath;
        mgFollow.ctime = 0.0f;
    }

    void Start()
    {
        partSystem.Play();
        started = true;
    }

    void Update() {
        if (started) {
            if (mgFollow.ctime >= mgFollow.time && mgFollow.loopmode != MegaRepeatMode.Loop && mgFollow.loopmode != MegaRepeatMode.PingPong)
            {
                mgFollow.distance = 0.0f;
                mgFollow.time = 0;
                mgFollow.ctime = 0.0f;
                partSystem.Stop();
            }
        }
    }
}
